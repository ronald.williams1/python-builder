# python-builder

Credit to Jeremy Satterfield for creating this. https://gitlab.com/jsatt

1. Clone this Dockerfile
1. `docker build . -t pythonbuild`
1. `docker run --rm --name pythonbuild -ti pythonbuild bash`
1. (in a different terminal) `docker cp pythonbuild:/build/python-3.9.10.tgz .`

Build Args
----------

    UBUNTU_VERSION (default 16.04)
    PYTHON_VERSION (default 3.9.10)

Use `--build-arg <ARG>=<value>` on `docker build` command to override. Be sure to use different
Python version on `docker cp` command.


Finishing Install
-----------------

1. Download tgz file
1. `tar -xzf python-3.9.10.tgz`
1. `cd python`
1. `make install`
