ARG UBUNTU_VERSION=16.04

FROM ubuntu:$UBUNTU_VERSION

RUN mkdir -p /build

WORKDIR /build

RUN apt-get update \
    && apt-get install -y make build-essential libssl-dev zlib1g-dev \
        libbz2-dev libreadline-dev libsqlite3-dev wget curl llvm \
        libncursesw5-dev xz-utils tk-dev libxml2-dev libxmlsec1-dev libffi-dev liblzma-dev

ARG PYTHON_VERSION=3.9.10

RUN wget https://www.python.org/ftp/python/${PYTHON_VERSION}/Python-${PYTHON_VERSION}.tgz -O python.tgz \
    && tar -xvf python.tgz \
    && rm python.tgz \
    && mv Python-${PYTHON_VERSION} python \
    && cd python \
    && ./configure \
    && make \
    && cd .. \
    && tar -czf python-${PYTHON_VERSION}.tgz python
